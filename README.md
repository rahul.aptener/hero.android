# BluConnect SDK

## Introduction

BluConnect is a SDK designed to help developers interact with BluArmor Devices using Bluetooth Low Energy technology. It simplifies the process of discovering, connecting, and communicating with these devices.

Key features of BluConnect include automatic device connection, device discovery, and the ability to check the status of the device's Bluetooth and location services. It also provides access to a list of previously connected devices and allows users to customize their preferences.

To use BluConnect in your Android project, you need to include the necessary library files and configure your project accordingly. Additionally, the SDK requires certain permissions, such as nearby device discovery, access to location, calendar, contacts, and physical activity, to function properly.

## Table of Contents

- [Getting Started](#getting-started)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [API Reference](#api-reference)

## Getting Started

Guide on how to get started with the SDK, including:
### Prerequisites

Before you begin integrating BluConnect into your Android project, ensure that you have the following prerequisites installed and configured:

#### Dependencies

- **Gradle**: Ensure your project is configured to use Gradle for dependency management.

####
- **Kotlin version**: 1.12.0
- **JVM target**: 17

#### Android SDK Compatibility

- **Minimum SDK Version**: 24
- **Target SDK Version**: 34

#### SDK Integration

- **SDK JAR/AAR File**: Add the SDK file (`*.jar` or `*.aar`) provided by BluConnect.

#### Permissions

- **Permissions**: Identify any permissions required by opted feature and ensure they are declared in the AndroidManifest.xml file of your project.

- Core SDK permission
```xml
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_CONNECTED_DEVICE" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_PHONE_CALL" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_LOCATION" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE_MICROPHONE" />
```

- SDK BLE permission
```xml
    <uses-feature android:name="android.hardware.bluetooth" android:required="true" />
    <uses-feature android:name="android.hardware.bluetooth_le" android:required="true" />

    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />

    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.BLUETOOTH_SCAN" tools:targetApi="s" />
    <uses-permission android:name="android.permission.BLUETOOTH_CONNECT" />
```

- Voice note recording permission
```xml
    <uses-permission android:name="android.permission.RECORD_AUDIO" />

    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
```

- Dialer & Speed dial permission
```xml
    <uses-permission android:name="android.permission.READ_CONTACTS" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
```

- Over the air device firmware update
```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

- Recommeded for optimal performance
```xml
    <uses-permission android:name="android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS" />
    <uses-permission android:name="android.permission.WAKE_LOCK" tools:ignore="SystemPermissionTypo" />
    
    <uses-permission android:name="android.permission.POST_NOTIFICATIONS" />
    <uses-permission android:name="android.permission.ACTIVITY_RECOGNITION" />
```


#### Initialization

- **Initialization Code**: Override `BluConnectService()` in the base app's long running service or create a new service if one does not exist.

#### Configuration

- **Configuration Settings**: Configure and provide at least [Core SDK permission](#core-sdk-permission) and [SDK BLE permission](#sdk-ble-permission) for proper functioning of the BluConnect SDK within your project.

## Installation

Detailed instructions on how to install the SDK, including:
- Copy all the libraries in _libs_ directory: `blueconnect.aar`, `rtk-bbpro-core-1.8.4.jar`, `rtk-core-1.5.15.jar`, `rtk-bbpro-1.12.9.aar` and `rtk-dfu-3.9.8.jar`
- Installation via Gradle
```groovy
    dependencies {
        implementation fileTree(dir: "libs", include: ["*.jar", "*.aar"])
    }
```
- Thirdparty dependencies:
```groovy
    /* play services */
    implementation 'com.google.android.gms:play-services-location:21.2.0'
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-play-services:1.8.0'

    /* dagger */
    implementation 'com.google.dagger:dagger:2.51'
    ksp 'com.google.dagger:dagger-compiler:2.51'

    /* NRF */
    implementation 'no.nordicsemi.android:ble-ktx:2.7.4'
    implementation 'no.nordicsemi.android.support.v18:scanner:1.6.0'
    implementation 'no.nordicsemi.android:dfu:2.4.2'

    /* Firebase */
    implementation platform('com.google.firebase:firebase-bom:32.7.4')
    implementation 'com.google.firebase:firebase-config-ktx'
    implementation 'com.google.firebase:firebase-storage-ktx'
    implementation 'com.google.firebase:firebase-analytics-ktx'
    implementation 'com.google.firebase:firebase-firestore-ktx'

    /* Protocol Buffer */
    implementation 'com.google.protobuf:protobuf-javalite:4.26.0'

    /* gson */
    implementation 'com.google.code.gson:gson:2.10.1'

    /* timber */
    implementation "com.jakewharton.timber:timber:5.0.1"

    /* room */
    implementation 'androidx.room:room-runtime:2.6.1'
    ksp 'androidx.room:room-compiler:2.6.1'
    implementation 'androidx.room:room-ktx:2.6.1'
```

## Configuration

Information on how to configure the SDK for different use cases, including:
- **Initialization**: Start by creating a new class that extends BluConnectService. This class will be a foreground service.
```kotlin
class BaseAppService:BluConnectService(){
    override fun onBind(intent: Intent?): IBinder? {
        ...
    }

    override fun onConnectionStateChange(state: ConnectionState) {
        ...
    }

    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmorDevice>) {
        ...
    }
}
```
- If a BluArmor device is discovered within Bluetooth range (approximately 10 meters), the connection procedure will commence automatically. As the connection establishes, any changes in `ConnectionState` will be promptly communicated through the onConnectionStateChange(...) method call. Following are the possible connection states.
```kotlin
    /**
    * BluConnect SDK connection state
    */
    sealed class ConnectionState(val device: BluArmorDevice) {

        /**
        * Connection request initiated
        */
        data class Connecting(private val _device: AdvertisingBluArmorDevice) : ConnectionState(_device)

        /**
        * Device is connected and discovering Services and Characteristics
        */
        data class Connected(private val _device: AdvertisingBluArmorDevice) : ConnectionState(_device)

        /**
        * Connection attempt failed
        * @param reason states the cause of failure
        *
        * @see FailReason for failure reason
        */
        data class Failed(private val _device: AdvertisingBluArmorDevice, @FailReason val reason: Int) :
            ConnectionState(_device) {
            companion object {
                /** The reason of disconnection is unknown.  */
                const val REASON_UNKNOWN = -1

                /** The disconnection was initiated by the user.  */
                const val REASON_SUCCESS = 0

                /** The local device initiated disconnection.  */
                const val REASON_TERMINATE_LOCAL_HOST = 1

                /** The remote device initiated graceful disconnection.  */
                const val REASON_TERMINATE_PEER_USER = 2

                /**
                * This reason will only be reported when connection to the device was lost. Android will try to connect automatically.
                */
                const val REASON_LINK_LOSS = 3

                /** The device does not hav required services.  */
                const val REASON_NOT_SUPPORTED = 4

                /**
                * The connection timed out. The device might have reboot, is out of range, turned off
                * or doesn't respond for another reason.
                */
                const val REASON_TIMEOUT = 10
            }
        }

        /**
        * Device is ready to operate.
        */
        data class DeviceReady(private val _device: ConnectedBluArmorDevice) : ConnectionState(_device)

        /**
        * In order for Bluetooth scan to work SDK require Location Permission.
        * It also require Location and Bluetooth to be turned On.
        */
        data class ScannerNotReady(val reason: NotReadyReason) : ConnectionState(NoDevice) {
            enum class NotReadyReason {
                NEARBY_PERMISSION_NEEDED,
                LOCATION_PERMISSION_NEEDED,
                LOCATION_IS_OFF,
                BLUETOOTH_IS_OFF,
                BLUETOOTH_IN_BAD_STATE
            }
        }

        /**
        * Device is disconnecting
        */
        data class Disconnecting(private val _device: AdvertisingBluArmorDevice) :
            ConnectionState(_device)

        /**
        * Device is not connected and scanning the devices
        */
        data object Discovery : ConnectionState(NoDevice)
    }
```

- Integration with WhatsApp: The announcement of WhatsApp messages depends on the `NotificationListenerService`. To enable these features, the main application must implement a NotificationListenerService and override two methods: onNotificationPosted and onNotificationRemoved.
```kotlin
    class MyNotificationListenerService : NotificationListenerService() {
        override fun onNotificationPosted(sbn: StatusBarNotification?) {
            super.onNotificationPosted(sbn)

            // Requied to parse WhatsApp messages and voice calls 
            BluConnectApp.onNotificationPosted(sbn)
        }

        override fun onNotificationRemoved(sbn: StatusBarNotification?) {
            super.onNotificationRemoved(sbn)

            // Requied to parse WhatsApp messages and voice calls
            BluConnectApp.onNotificationRemoved(sbn)
        }
    }
```

## Usage

- Identify when device is paired: The `onConnectionStateChange(...)` event emits the ConnectionState.Ready state, it indicates that the device is successfully connected and ready for operation.
- To obtain the connected BluArmor device instance, utilize either `ConnectionState.DeviceReady.device` or the `getActiveDevice()` method from `BluConnectService`.
```kotlin
    when (connectionState) {
        is ConnectionState.DeviceReady -> {
            if (state.device.model == BluArmorModel.HS1) {
                val hS1Device: HS1? = state.device as? HS1
                // Update the UI
            }
        }
        ...
    }
```

## API Reference

Detailed documentation of all classes, methods, and attributes provided by the SDK, organized into sections such as:

#### BluConnect SDK provides access to various features and controls of BluArmor HS1.
 * [DeviceInfoHS1]() Version metadata
 * [Battery]() Battery updates
 * [Volume]() Offers volume controls
 * [MediaPlayer]() Offers media player controls
 * [Dialer]() Offers phone call controls
 * [VoiceAnnouncement]() Play custom text announcements and media files
 * [VoiceNoteRecorder]() Records voice note with location data
 * [VoiceAssistant]() Invoke voice assistant
 * [ButtonClickHS1]() Observe button clicks
 * [DialerSpeedDial]() Speed dial intercom
 * [OtaSwitch]() Start OTA update

### Battery
Once the device is paired, the battery level updates with each alteration in level and whenever the charger is plugged in or disconnected.

- **Emits State**: `BatteryLevel(Int, ChargingState)`
```kotlin
    enum class ChargingState {
        UNKNOWN, CHARGING, FULL, DRAINING
    }
```
- **To observe** [BatteryLevel]()
```kotlin
    private val batteryObserver = PropertyObserver<BatteryLevel>{ batterLevel ->
        //Update UI
    }

    hs1.battery.observe(batteryObserver)
```

### Dialer
Dialer helps manage phone calls on a device. It provides functionality to interact with incoming and outgoing calls, such as answering, dropping, and rejecting calls.

- **Emits State**: `DialerState`
    - Unknown: Represents an undetermined state of the phone call, default if state cannot be determined.

    - Idle: Represents no ongoing call activity.

    - Missed: Represents a missed incoming call.
    
    - Incoming: Data class containing info about incoming call (phone number, name).

    - Active: Data class containing info about ongoing active call (phone number, name).

    - Outgoing: Data class containing info about outgoing call (phone number, name).
<br>*_phone number and name if available of the caller_

- **To observe** [DialerState]()
```kotlin
    private val dialerStateObserver = PropertyObserver<Dialer.DialerState>{ dialerState ->
        //Update UI
    }

    hs1.dialer.observe(dialerStateObserver)
```

- **Functions to manage phone calls**
    * acceptPhoneCall(): This function answers an incoming phone call.

    * endPhoneCall(): This function terminates an ongoing phone call.

    * rejectPhoneCall(): This function declines or rejects an incoming phone call.

### MediaPlayer
MediaPlayer defines an interface for controlling media playback. It includes methods for playing, pausing, moving to the next and previous tracks, and observing changes in the media player's state

- **Emits State**: `MediaPlayerState`

    - Idle: This state indicates that no media is currently playing or paused. It signifies a state of inactivity or standby where the media player is not engaged in playback.

    - Playing: This state signifies that media is actively playing. It includes metadata about the currently playing track and artist. The metadata parameters `track` and `artist` provide information about the currently active music being played.

- **To observe** [MediaPlayerState]()
```kotlin
    private val mediaPlayerStateObserver = PropertyObserver<MediaPlayer.MediaPlayerState>{ state ->
        //Update UI
    }

    hs1.mediaPlayer.observe(mediaPlayerStateObserver)
```

- **Functions to control media playback**

    - playMusic(): Plays music when the device is idle.

    - pauseMusic(): Pauses the music if music is currently playing.

    - nextMusic(): Changes the music to the next track.

    - previousMusic(): Changes the music to the previous track.

### Volume
Volume defines an interface which handles volume control for a device. It emits information about the volume level. It provides methods to observe changes in the volume level and to update various aspects of volume control


- **Emits State**: `VolumeLevel`

    - volumeLevel: Integer ranges from 0 to 15.

    - dynamicMode: Boolean state signifies that volume will be controlled with respect to riding speed.

- **To observe** [VolumeLevel]()
```kotlin
    private val volumeLevelObserver = PropertyObserver<Volume.VolumeLevel>{ volumeLevel ->
        //Update UI
    }

    hs1.volume.observe(volumeLevelObserver)
```

- **Functions to control volume level**

    - changeVolume(level: Int): Updates the volume level to the desired value within the range of 0 to 15.

    - enableDynamicMode()

    - disableDynamicMode()


### In addition to above feture sets there are system level control to `power OFF`, `reboot`, `rename` and `factory reset` the HS1
```kotlin 
    /**
     * Turn off the device.
     * This function turns off the Smart Helmet Headset HS1 device.
     */
    fun turnOff()

    /**
     * Restart the device.
     * This function restarts the Smart Helmet Headset HS1 device.
     */
    fun reboot()

    /**
     * Change the device name.
     *
     * @param name The new device name. "HS1_" will be auto appended before the name.
     * This function changes the name of the Smart Helmet Headset HS1 device.
     */
    fun rename(name: String)

    /**
     * Perform a factory reset, wiping all data from the device.
     * This function resets the Smart Helmet Headset HS1 device to its factory settings,
     * erasing all stored data.
     */
    fun factoryReset()
```


_Additionally, using JavaDoc references will be advantageous for implementation guidance._