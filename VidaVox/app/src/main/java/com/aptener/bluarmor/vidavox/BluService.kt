package com.aptener.bluarmor.vidavox

import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.aptener.bluconnect.BluArmorModel
import com.aptener.bluconnect.BluConnectService
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.device.AdvertisingBluArmorDevice
import com.aptener.bluconnect.device.BluArmorDevice
import com.aptener.bluconnect.device.VidaVoxV1
import com.aptener.bluconnect.device.control.FirmwareUpdate
import com.aptener.bluconnect.device.control.OtaBinaryPath
import com.aptener.bluconnect.device.control.OtaSwitch
import kotlinx.coroutines.flow.MutableStateFlow
import timber.log.Timber

class BluService : BluConnectService() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onBluArmorDeviceDiscovered(result: Set<AdvertisingBluArmorDevice>) {

//        Timber.e("onBluArmorDeviceDiscovered: $result")

    }

    override fun onConnectionStateChange(state: ConnectionState) {
        Timber.e("onConnectionStateChange: $state")
        BluService.onConnectionStateChangeListener?.invoke(state)
        when (state) {
            is ConnectionState.DeviceReady -> {
                BluService.device = state.device
                when {
                    device?.model == BluArmorModel.VIDA_VOX_V1 -> {

                        otaProgress.value = 0

                    }

                    else -> Log.e("CONNECTION", "model -> ${device?.model}")

                }
            }

            else -> Unit
        }
    }

    companion object {
        private var device: BluArmorDevice? = null

        var otaState: MutableStateFlow<Boolean> = MutableStateFlow(true)
        var otaProgress: MutableStateFlow<Int> = MutableStateFlow(0)

        var onConnectionStateChangeListener: ((state: ConnectionState) -> Unit)? = null
        fun getDevice(): BluArmorDevice? = BluService.device
    }

    override fun onBind(intent: Intent): IBinder = binder

    private val binder = BluServiceBinder()

    inner class BluServiceBinder : Binder() {
        val service
            get() = this@BluService
    }
}