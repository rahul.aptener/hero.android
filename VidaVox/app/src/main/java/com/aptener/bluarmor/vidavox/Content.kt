package com.aptener.bluarmor.vidavox


import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun ContentView(vm: MainViewModel) {

    val connected = vm.connected


    val mediaPlayerState by vm.mediaPlayerState.collectAsState()

    val title by vm.title.collectAsState()
    val artist by vm.artist.collectAsState()



    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(top = 50.dp)
            .padding(20.dp)
            .fillMaxSize()
            .clip(RoundedCornerShape(30.dp))
            .background(Color.Black.copy(alpha = 0.06f))
            .padding(20.dp)
    ) {
        Text(
            text = if (connected) "Connected" else "Disconnected",
            color = Color.White,
            fontWeight = FontWeight.SemiBold,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(vertical = 20.dp)
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
                .height(70.dp)
                .clip(RoundedCornerShape(20.dp))
                .background(if (connected) Color.Green else Color.Red)
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = title,
                textAlign = TextAlign.Center
            )
            Text(
                text = artist,
                textAlign = TextAlign.Center
            )
        }


        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)
                .background(Color.LightGray, shape = RoundedCornerShape(30.dp)),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(imageVector = ImageVector.vectorResource(id = R.drawable.ic_previous),
                contentDescription = "Previous",
                modifier = Modifier
                    .scale(1.5f)
                    .clickable {
                        vm.mediaPlayerSwitch(MediaPlayerAction.PREVIOUS)
                    }
            )
            Spacer(modifier = Modifier.width(50.dp))
            Image(imageVector = ImageVector.vectorResource(
                id = if (mediaPlayerState) R.drawable.ic_pause else R.drawable.ic_play_arrow
            ),
                contentDescription = "Play",
                modifier = Modifier
                    .scale(1.5f)
                    .clickable {
                        if (mediaPlayerState) {
                            vm.getMediaPlayerState()
                            vm.mediaPlayerSwitch(MediaPlayerAction.PAUSE)
                        } else {
                            vm.getMediaPlayerState()
                            vm.mediaPlayerSwitch(MediaPlayerAction.PLAY)

                        }
                    }
            )
            Spacer(modifier = Modifier.width(50.dp))
            Image(imageVector = ImageVector.vectorResource(id = R.drawable.ic_next),
                contentDescription = "Next",
                modifier = Modifier
                    .scale(1.5f)
                    .clickable {
                        vm.mediaPlayerSwitch(MediaPlayerAction.NEXT)
                    }

            )

        }


    }
}

@Preview(showBackground = true)
@Composable
fun ContentViewPreview() {
    ContentView(vm = MainViewModel())
}
