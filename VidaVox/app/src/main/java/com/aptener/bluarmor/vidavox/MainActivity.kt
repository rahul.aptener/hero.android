package com.aptener.bluarmor.vidavox

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import com.aptener.bluarmor.vidavox.ui.theme.VidaVoxTheme
import kotlinx.coroutines.flow.MutableStateFlow
import timber.log.Timber

class MainActivity : ComponentActivity(), ServiceConnection {

    private val bluService: MutableStateFlow<BluService?> = MutableStateFlow(null)

    private val vm = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            VidaVoxTheme {
                ContentView(vm)
            }
        }
    }

    override fun onStart() {

        Timber.plant(Timber.DebugTree())

        with(Intent(this, BluService::class.java)) {
            startService(this)
        }

        // Bind to LocalService
        Intent(this, BluService::class.java).also { intent ->
            Timber.e("Binding service")
            bindService(intent, this, Context.BIND_AUTO_CREATE)
        }

        super.onStart()
    }

    override fun onStop() {
        unbindService(this)
        super.onStop()
    }

    override fun onServiceConnected(className: ComponentName, service: IBinder) {
        val binder = service as BluService.BluServiceBinder
        Timber.e("onServiceConnected: $binder")
        bluService.tryEmit(binder.service)
    }

    override fun onServiceDisconnected(className: ComponentName) {
        bluService.tryEmit(null)
    }
}


