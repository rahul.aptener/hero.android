package com.aptener.bluarmor.vidavox

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aptener.bluconnect.client.ConnectionState
import com.aptener.bluconnect.common.usecase.PropertyObserver
import com.aptener.bluconnect.device.VidaVoxV1
import com.aptener.bluconnect.device.control.MediaPlayer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

enum class MediaPlayerAction {
    PLAY,
    PAUSE,
    NEXT,
    PREVIOUS
}


class MainViewModel : ViewModel() {

    init {
        BluService.onConnectionStateChangeListener = this::onConnectionStateChange
    }

    private val _title = MutableStateFlow("")
    val title = _title.asStateFlow()

    private val _artist = MutableStateFlow("")
    val artist = _artist.asStateFlow()

    private val _mediaPlayerState = MutableStateFlow(false)
    val mediaPlayerState = _mediaPlayerState.asStateFlow()


    private val mediaPlayerStateObserver = PropertyObserver<MediaPlayer.MediaPlayerState> { state ->
        Timber.e("MediaPlayerState Observer: $state")
        if (state is MediaPlayer.MediaPlayerState.Playing) {
            Timber.e("MediaPlayerState Observer: ${state.track} by ${state.artist}")
            setTitle(state.track.toString())
            setArtist(state.artist.toString())
            setMediaPlayerState(true)
        } else {
            setMediaPlayerState(false)
            setTitle("")
            setArtist("")
        }
    }

    var connected by mutableStateOf(false)
        private set

    fun getActiveDevice(): VidaVoxV1? {
        return BluService.getDevice() as? VidaVoxV1
    }

    fun mediaPlayerSwitch(action: MediaPlayerAction) {
        when (action) {
            MediaPlayerAction.PLAY -> getActiveDevice()?.mediaPlayer?.playMusic()
            MediaPlayerAction.PAUSE -> getActiveDevice()?.mediaPlayer?.pauseMusic()
            MediaPlayerAction.NEXT -> getActiveDevice()?.mediaPlayer?.nextMusic()
            MediaPlayerAction.PREVIOUS -> getActiveDevice()?.mediaPlayer?.previousMusic()
        }
    }

    fun onConnectionStateChange(state: ConnectionState) {
        Timber.e("onConnectionStateChange: $state")
        viewModelScope.launch {
            when (state) {
                is ConnectionState.DeviceReady -> {
                    Timber.e("Device Ready")
                    connected = true
                    getMediaPlayerState()
                }

                else -> {
                    connected = false
                }
            }
        }
    }

    fun getMediaPlayerState() {
        getActiveDevice()?.mediaPlayer?.observe(mediaPlayerStateObserver)
    }

    fun setTitle(title: String) {
        _title.value = title
    }

    fun setArtist(artist: String) {
        _artist.value = artist
    }

    fun setMediaPlayerState(mediaPlayerState: Boolean) {
        _mediaPlayerState.value = mediaPlayerState
    }


}
